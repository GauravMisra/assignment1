from flask import Flask,request,jsonify
from connection import CreateDB,Manager
from connection import db
app = Flask(__name__)

@app.route("/v1/expenses",methods=['POST'])
def postdb():
 CreateDB()
 db.create_all()
 getdata=request.get_json(force=True)
 name=getdata['name']
 #print "name inserted"
 email=getdata['email']
 #print "email"
 category=getdata['category']
 #print "category"
 description=getdata['description']
 estimated_costs=getdata['estimated_costs']
 link=getdata['link']
 submit_date=getdata['submit_date']
 status='pending'
 decision_date=''
 #print estimated_costs
 m1=Manager(name, email, category, description, estimated_costs, link, submit_date, status, decision_date)
 db.session.add(m1)
 db.session.commit()
 reply={'id':m1.id,
        'name':m1.name,
        'email':m1.email,
        'category':m1.category,
        'description':m1.description,
        'estimated_costs':m1.estimated_costs,
        'link':m1.link,
        'submit_date':m1.submit_date,
        'status':m1.status,
        'decision_date':m1.decision_date
        }
 return jsonify(reply),201
@app.route("/v1/expenses/<int:num>", methods=['GET','PUT','DELETE'])
def getdb(num):

	m2 = Manager.query.filter_by(id=num).first_or_404()
 	if request.method=='GET':
   		reply2={'name': m2.name,
         'email': m2.email,
         'category': m2.category,
         'description': m2.description,
         'estimated_costs': m2.estimated_costs,
         'link': m2.link,
         'submit_date': m2.submit_date,
         'status': m2.status,
         'decision_date': m2.decision_date}
 		return jsonify(reply2)
	elif request.method=='PUT':
		getdata=request.get_json(force=True)
 		m2.estimated_costs=getdata['estimated_costs']
 		db.session.commit()
		return "DB changed",202
 	else:
 		db.session.delete(m2)
 		db.session.commit()
		return "DB updated",204

if __name__ == '__main__':
 app.run('0.0.0.0', debug=True)
