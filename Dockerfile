FROM python:2.7
MAINTAINER Gaurav Misra "gauravmisra544@gmail.com"
COPY . /assignment1
WORKDIR /assignment1
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["submit.py"]
